package com.xzbd.mail.utils.mailContentUtil;

import javax.mail.BodyPart;
import javax.mail.MessagingException;
import java.io.UnsupportedEncodingException;

public abstract class  AbstractEmailContentItem {
    private BiEmailContentTypeEnum contentType;

    public BiEmailContentTypeEnum getContentType() {
        return contentType;
    }

    public void setContentType(BiEmailContentTypeEnum contentType) {
        this.contentType = contentType;
    }

    public abstract BodyPart getMailBodyPart() throws MessagingException, UnsupportedEncodingException;

}
