package com.xzbd.mail.utils.serviceUtil;

import com.xzbd.mail.enums.ReceiverTypeEnum;
import org.apache.commons.lang.StringUtils;

import java.util.Arrays;
import java.util.stream.Collectors;

public class MailServiceUtils {
    public static String getNumberIds(String originIds, ReceiverTypeEnum type) {
        return StringUtils.join(Arrays.stream(originIds.split(","))
                .filter(o ->{
                    if(ReceiverTypeEnum.CUSTOM.equals(type)){
                        return  o.contains(type.name().toLowerCase()) || StringUtils.isNumeric(o);
                    }else {
                        return  o.contains(type.name().toLowerCase());
                    }
                })
                .map(o -> {
                    String[] str = ((String) o).split("_");
                    return str[0];
                })
                .collect(Collectors.toList()), ",");
    }


}
